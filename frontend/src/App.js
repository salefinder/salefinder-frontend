import React from 'react';
import './styles/index.css';

// Routing
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

// Components
import Header from './components/Header';
import Footer from './components/Footer';

// Pages
import HomePage from './pages/home';
import ContactPage from './pages/contact';
import AboutPage from './pages/about';
import StorePage from './pages/stores';

function App() {
    return (
        <div className="App">
            <Router>
                <Header />
                <div id='bodyContainer'>
                    <Switch>
                        <Route path='/contact'>
                            <ContactPage />
                        </Route>
                        <Route path='/about'>
                            <AboutPage />
                        </Route>
                        <Route path='/stores'>
                            <StorePage />
                        </Route>
                        <Route path='/'>
                            <HomePage />
                        </Route>
                    </Switch>
                </div>
                <Footer />
            </Router>
        </div>
    );
}

export default App;
