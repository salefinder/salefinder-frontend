import React from 'react';
import '../styles/contact.css';

// Material UI
import {
    TextField,
    Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiFilledInput-root': {
            marginBottom: '5px',
            color: '#333333',
            fontSize: '20px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#999999'
        },
        '& .MuiButton-root': {
            marginTop: '10px',
            padding: '16px 15px 16px 15px',
            border: 0,
            color: '#ffffff',
            backgroundColor: '#e0208a',
            justifyContent: 'center',
            fontSize: '17px',
            float: 'right',
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#e754a5',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        // Mobile screens
        [theme.breakpoints.down('sm')]: {
            '& .MuiFilledInput-root': {
                marginRight: 0,
                fontSize: '16px',
            },
            '& .MuiButton-root': {
                marginTop: '10px',
                fontSize: '15px',
                float: 'right',
                display: 'inline-block',
            },
        },
        justifyContent: 'flex-end'
      
    },
}));

function Contact(props) {
    const classes = useStyles();
    const {
        handleFormUpdate,
        handleSubmit,
        emailHasError,
        messageHasError,
        contactErrorMessage,
        contactSuccess,
    } = props;

    // Message was successfully sent, hide this form now
    if (contactSuccess) {
        return '<div>Your message has been sent to us!<div>';
    }

    return (
        <div id='contactFormContainer'>
            <div id='contactHeader'>Any stores you would like to see? Send us a message</div>
            <form id='contactForm' className={classes.root}>
                <TextField
                    variant='filled'
                    label='Email'
                    name='contactEmail'
                    placeholder='jason@bourne.com'
                    InputProps={{ className: classes.input }}
                    onChange={handleFormUpdate}
                    onKeyPress={(event) => event.key === 'Enter' ? handleSubmit(event) : null }
                    error={emailHasError}
                    fullWidth={true}
                />

                <TextField
                    variant='filled'
                    label='Message'
                    name='contactMessage'
                    placeholder="Jesus Christ it's Jason Bourne"
                    InputProps={{ className: classes.input }}
                    onChange={handleFormUpdate}
                    error={messageHasError}
                    fullWidth={true}
                    rows={2}
                    rowsMax={4}
                    multiline
                />

                {contactErrorMessage ? <div style={{ color: '#DC143C', margin: '10px 0 10px 0' }}>{contactErrorMessage}</div> : null}

                <Button
                    type='button'
                    variant='outlined'
                    InputProps={{ className: classes.input }}
                    onClick={handleSubmit}
                >
                    Send
                </Button>
            </form>
        </div>
    )
}

export default Contact;