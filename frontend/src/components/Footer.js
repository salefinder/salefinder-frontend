import React from 'react';
import '../styles/footer.css';

import { Link } from 'react-router-dom';

function Footer() {
    return (
        <div id='footerContainer'>
            <div id='footerBody'>
                <div id='footerLogoContainer'>
                    <Link id='footerLogo' to='/' className='link'>Dealyy</Link>
                    BETA
                </div>
                <div id='footerLinksContainer'>
                    {/* <div className='headerItem'><Link to='/about' className='link'>About</Link></div> */}
                    <div className='footerItem'><Link to='/contact' className='link'>Contact</Link></div>
                </div>
            </div>
        </div>
    )
}

export default Footer;