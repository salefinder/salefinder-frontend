import React from 'react';
import '../styles/header.css';

import { Link } from 'react-router-dom';

function Header() {
    return (
        <div id='headerContainer'>
            <div id='headerBody'>
                <div id='headerLogoContainer'>
                    <Link id='headerLogo' to='/' className='link'>Dealyy</Link>
                    BETA
                </div>
                <div id='headerLinksContainer'>
                    {/* <div className='headerItem'><Link to='/about' className='link'>About</Link></div> */}
                    <div className='headerItem'><Link to='/stores' className='link'>Stores</Link></div>
                    <div className='headerItem'><Link to='/contact' className='link'>Contact</Link></div>
                </div>
            </div>
        </div>
    )
}

export default Header;