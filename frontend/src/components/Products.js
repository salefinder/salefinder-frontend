import React, { useState, useEffect } from 'react';
import categories from '../categories.json';

// Components
import ProductsHeader from './ProductsHeader';
import ProductsList from './ProductsList';

// Material UI
import {
    CircularProgress,
} from '@material-ui/core';

// Utils
import { sortProducts } from '../utils/sorts';
import { capitalize } from '../utils/strings';

// fuzzy search
import Fuse from 'fuse.js';
import _ from 'lodash';

// Styles
import '../styles/products.css';

function Products() {
    const [loading, setLoading] = useState(true);
    
    // Products states
    const [allProducts, updateAllProducts] = useState([]);
    const [displayedProducts, updateDisplayedProducts] = useState([]);
    const [remainingProducts, updateRemainingProducts] = useState(null);
    const [totalProductsCount, updateTotalProductsCount] = useState(0);
    const [productsLoaded, updateProductsLoaded] = useState(20);
    const [hasDisplayChanged, updateHasDisplayChanged] = useState(false);

    // Shows/hide filter options
    const [productsOptionsVisible, updateProductsOptionsVisible] = useState(false);

    // Filter states
    const [hasFilterChanged, updateHasFilterChanged] = useState(false);
    const [allUniqueStores, updateAllUniqueStores] = useState({});
    const [chosenFilteredStores, updatechosenFilteredStores] = useState([]);
    const [genderSelectVisible, updateGenderSelectVisible] = useState(false);
    const [genderChosen, updateGenderChosen] = useState('both');
    const [hasFilterApplied, updateHasFilterApplied] = useState(false);

    // Sort states
    const [currentSortOption, updateCurrentSortOption] = useState('default');
    const [hasSortChanged, updateHasSortChanged] = useState(false);

    // Search
    const [searchValue, updateSearchValue] = useState('');

    // Anchor
    const [anchorTarget, setAnchorTarget] = useState(null);

    // Categories
    const [categoryTypes, updateCategoryTypes] = useState([]);
    const [categorySubTypes, updatecategorySubTypes] = useState([]);
    const [chosenCategoryType, updateChosenCategoryType] = useState('all');
    const [chosencategorySubTypes, updatechosencategorySubTypes] = useState([]);
    const [chosenTags, updateChosenTags] = useState([]);
    
    const getAllProducts = () => {
        const url = 'https://dealyy.com/api/products/all?limit=50000';

        // Send request to get products
        fetch(url)
            .then(response => response.json())
            .then(response => {
                // Got products successfully
                const productResults = response.allProducts;
                const totalProducts = response.totalProducts;
                
                // Save all products, total counts
                updateAllProducts(productResults);
                updateTotalProductsCount(totalProducts);

                // Filter and sort displayed products
                updateDisplayedProducts(productResults.slice(0, productsLoaded));
            })
            .catch(err => console.log(err));
    };

    // Filter down products from allProducts using selected filters
    const getShownProducts = (productsList) => {        
        // Filter stores
        const remainingResults = filterStores(productsList)

        // If no results after filtering show 'no products' message
        if (!remainingResults) {
            updateDisplayedProducts([]);
        } else {
            //Update total amount of products after filters
            updateRemainingProducts(remainingResults.length);

            // Sort & update displayed products
            const sortedProducts = sortProducts(currentSortOption, remainingResults).slice(0, productsLoaded)
            updateDisplayedProducts(sortedProducts);
        }; 
    };

    // Load store names for filter
    const getFilterStoreOptions = productResults => {
        // Get individual brands for filter
        // We use dict like a tuple to remove dupes
        const uniqueStores = allUniqueStores;
        productResults.map(store => {
            const brand = capitalize(store.brand)
            uniqueStores[brand] = null
        });
        updateAllUniqueStores(uniqueStores);
    }

    // Filter stores
    const filterStores = productsList => {
        let results = productsList.filter(product => {
            // Show all stores, no stores selected
            if (chosenFilteredStores.length === 0) {
                return product;
            };

            // Show selected stores
            if (chosenFilteredStores.includes(capitalize(product.brand))) {
                return product;
            };

            return null;
        });

        // Filter by gender
        results = results.filter(product => {
            if (genderChosen === 'both') {
                return product;
            } else if (product.sex === genderChosen) {
                return product;
            };
            return null;
        });

        // Filter by category
        results = results.filter(product => {
            // No category chosen, show all products
            if (chosenCategoryType === 'all') {
                return product;
            } else if (!product.type) {
                // Missing type parameter
                return null;
            } else if (product.type !== chosenCategoryType) {
                // Not in category chosen
                return null
            } else if (chosencategorySubTypes.length === 0) {
                // No subtype chosen yet, show all within this category
                return product;
            } else if (!product.tags) {
                // Missing tags parameter
                return null;
            } else {
                const matchingTags = product.tags.filter(tag => chosenTags.includes(tag));
                if (matchingTags.length !== 0) return product;
                return null; // no match tags
            };
        });

        // Search results
        if (searchValue) {
            results = productSearch(searchValue, results);
        };

        return results;
    };

    //Filters by store
    const handleStoreFilter = (event, selectedStores) => {
        updatechosenFilteredStores(selectedStores);

        // Reset displayed products back to default
        updateProductsLoaded(20);

        // Trigger filter update
        updateHasFilterChanged(true);
    };

    // Handle gender filter
    const handleGenderFilter = event => {
        const gender = event.target.value;
        updateGenderChosen(gender);

        // Reset displayed products back to default
        updateProductsLoaded(20);

        // Trigger filter update
        updateHasFilterChanged(true);
    };

    // Handle search change
    const handleSearchChange = (event) => {
        const searchWords = event.target.value;
            updateSearchValue(searchWords);
            updateHasFilterChanged(true);
    };

    // Search 
    const productSearch = (searchWords, productsList) => {
        const options = {
            includeScore: true,
            ignoreLocation: true,
            threshold: .225,
            keys: ['name']
        };
        const fuse = new Fuse(productsList, options);
        return fuse.search(searchWords).map((searchItem) => searchItem.item);
    };
    
    // Handle sort update
    const handleSortChange = event => {
        updateCurrentSortOption(event.target.value);

        // Reset displayed products back to default
        updateProductsLoaded(20);
        updateHasSortChanged(true);
    };

    // Handle category type change
    const handleCategoryTypeUpdate = categoryType => {
        const oldCategoryChosen = chosenCategoryType;

        if (oldCategoryChosen === categoryType) {
            updateChosenCategoryType('all')
            categoryType = 'all'
        } else {
            updateChosenCategoryType(categoryType);
        }

        // No main category selected, cannot have subTypes
        if (categoryType === 'all') {
            updatecategorySubTypes([])
        } else {
            // Show gender if clothing type selected
            if (categoryType == 'clothing') {
                updateGenderSelectVisible(true)
            } else {
                updateGenderSelectVisible(false)
            }

            // Update subTypes & clear any old chosen subTypes
            const subTypes = Object.keys(categories[categoryType]).map(subtype => subtype);
            updatecategorySubTypes(subTypes);
            updatechosencategorySubTypes([]);
        }

        // Reset displayed products back to default && trigger update
        updateProductsLoaded(20);
        updateHasFilterChanged(true);
    };

    // Handle category change
    const handleCategorySubtypeUpdate = subTypeName => {
        // For easy manipulation
        let newCategories = chosencategorySubTypes;

        // Remove from list
        if (chosencategorySubTypes.includes(subTypeName)) {
            newCategories = chosencategorySubTypes.filter(name => name !== subTypeName);
        } else {
            newCategories.push(subTypeName);
        };

        // Update in state
        updatechosencategorySubTypes(newCategories)

        // List of tags we will use for filtering
        // Tags come from categories: tops, bottoms, outerwear
        let tagsToAdd = [];

        // Update list of tags to look filter by
        for (let subType in newCategories) {
            const subTypeName = newCategories[subType]
            if (newCategories.includes(subTypeName)) {
                const newTags = categories[chosenCategoryType][subTypeName]
                tagsToAdd = tagsToAdd.concat(newTags);
            }
        };

        // Update tags state
        updateChosenTags(tagsToAdd);

        // Reset displayed products back to default && trigger update
        updateProductsLoaded(20);
        updateHasFilterChanged(true);
    };

    // Handle displaying more products
    const handleDisplayMoreProducts = () => {
        updateProductsLoaded(productsLoaded + 20);
        updateHasDisplayChanged(true);
    };

    // Display more products when 'load more' button is visible
    const handleVisibilityChange = (isVisible) => {
        if (isVisible) handleDisplayMoreProducts();
    };

    // Handle a filter update
    useEffect(() => {
        // There is a filter update
        if (hasFilterChanged || hasSortChanged) {            
            // Apply filters and sorts
            getShownProducts(allProducts);
            
            // Scroll to top of list
            anchorTarget.scrollIntoView({ behavior: 'smooth', block: 'start' });

            // Reset triggers
            updateHasFilterChanged(false);
            updateHasSortChanged(false);
            updateHasDisplayChanged(false);

            // Determine what total products count to use
            if (chosenFilteredStores.length !== 0
                || genderChosen !== 'both'
                || chosenCategoryType !== 'all'
                || searchValue != ''
            ) {
                updateHasFilterApplied(true);
            } else {
                updateHasFilterApplied(false);
            };
        };
    }, [hasFilterChanged, hasSortChanged]);
    
    // Load more products on scroll
    useEffect(() => {
        if (hasDisplayChanged) {
            getShownProducts(allProducts);
            updateHasDisplayChanged(false);
        };
    }, [hasDisplayChanged]);

    // Handle getting products from API
    useEffect(() => {
        if (loading) {
            // Gets products from backend
            getAllProducts();

            // Load in category types
            updateCategoryTypes(Object.keys(categories).filter(type => Object.keys(categories[type]).length > 0));
            
            // Hide loading message
            setLoading(false);
        }
    }, [loading]);

    // All products has changed
    useEffect(() => {
        // Load store names into filter, sort by most popular
        getFilterStoreOptions(allProducts);
    }, [allProducts])

    useEffect(() => {
        setAnchorTarget(document.getElementById('productsContainer'));
    });

    // Show loading message while getting products
    if (loading || allProducts.length === 0) {
        return (
            <div className='loadingMessageContainer'>
                Loading products...
                <CircularProgress />
            </div>
        );
    };

    return (
        <div id='productsContainer'>
            <div id='productsHeaderContainer'>
                <ProductsHeader
                    allUniqueStores={allUniqueStores}
                    handleStoreFilter={handleStoreFilter}
                    genderSelectVisible={genderSelectVisible}
                    genderChosen={genderChosen}
                    handleGenderFilter={handleGenderFilter}
                    currentSortOption={currentSortOption}
                    handleSortChange={handleSortChange}

                    // Categories
                    categoryTypes={categoryTypes}
                    categorySubTypes={categorySubTypes}
                    chosenCategoryType={chosenCategoryType}
                    chosencategorySubTypes={chosencategorySubTypes}
                    handleCategoryTypeUpdate={handleCategoryTypeUpdate}
                    handleCategorySubtypeUpdate={handleCategorySubtypeUpdate}

                    // Search
                    handleSearchChange={handleSearchChange}

                    productsOptionsVisible={productsOptionsVisible}
                    updateProductsOptionsVisible={updateProductsOptionsVisible}
                />
            </div>

            <div id='productsCounts'>
                {
                    hasFilterApplied
                    ? `Showing ${displayedProducts.length} of ${remainingProducts}`
                    : `Showing ${displayedProducts.length} of ${totalProductsCount} `
                }
            </div>
            
            <div id='productsListContainer'>
                <ProductsList
                    displayedProducts={displayedProducts}
                    remainingProducts={remainingProducts}
                    sortProducts={sortProducts}
                    handleDisplayMoreProducts={handleDisplayMoreProducts}
                    handleVisibilityChange={handleVisibilityChange}
                />
            </div>
        </div>
    )
}

export default Products;