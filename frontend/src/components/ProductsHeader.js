import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import '../styles/productsHeader.css';

// Material UI, Lab
import { 
    Chip,
    TextField,
    Checkbox,
    Select,
    MenuItem,
    InputLabel,
    InputAdornment,
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import TuneIcon from '@material-ui/icons/Tune';
import { Search } from '@material-ui/icons';
import { VariableSizeList } from 'react-window';

// Utils
import { sortProducts } from '../utils/sorts';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: '250px',
        marginRight: '10px',
        // Mobile screens
        [theme.breakpoints.down('sm')]: {
            width: '62vw',
            maxWidth: '100%',
        },
    },
    search: {
        width: '100%',
        maxWidth: '400px',
        // Mobile screens
        [theme.breakpoints.down('sm')]: {
            width: '80vw',
            maxWidth: '100%',
        },
    },
    select: {
        fontSize: '16px',
        margin: 0,
    },
    selectLabel: {
        width: '100%',
        marginBottom: '2px',
        fontSize: '13px',
        textAlign: 'left',
    },
    categoryChip: {
        margin: '0 5px 5px 0',
        fontSize: '11px',
        borderRadius: '5px',
    },
}));

// MATERIAL UI CRAP FOR AUTOCOMPLETE VIRTUAL LOADING
function renderRow(props) {
  const { data, index, style } = props;
  return React.cloneElement(data[index], {
    style: {
      ...style,
      top: style.top,
    },
  });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
  const ref = React.useRef(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

// Adapter for react-window
const ListboxComponent = React.forwardRef(function ListboxComponent(props, ref) {
  const { children, ...other } = props;
  const itemData = React.Children.toArray(children);
  const itemCount = itemData.length;
  const itemSize = 36;

  const getChildSize = (child) => {
    if (React.isValidElement(child) && child.type) {
      return 48;
    }

    return itemSize;
  };

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }
    return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
  };

  const gridRef = useResetCache(itemCount);

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
            style={{ overflowX: 'hidden' }}
            itemData={itemData}
            height={getHeight() + 2}
            width="100%"
            ref={gridRef}
            outerElementType={OuterElementType}
            innerElementType="ul"
            itemSize={(index) => getChildSize(itemData[index])}
            overscanCount={5}
            itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

function ProductsHeader(props) {
    const classes = useStyles();
    const { 
        allUniqueStores,
        handleStoreFilter,
        genderSelectVisible,
        genderChosen,
        handleGenderFilter,
        currentSortOption,
        handleSortChange,

        // Categories
        categoryTypes,
        categorySubTypes,
        chosenCategoryType,
        chosencategorySubTypes,
        handleCategoryTypeUpdate,
        handleCategorySubtypeUpdate,

        // Search
        handleSearchChange,

        productsOptionsVisible,
        updateProductsOptionsVisible,
    } = props;

    // Create menuitem for each type
    const categoryTypesMenuItems = categoryTypes.map(categoryName => {
        return (
            <Chip
                key={categoryName}
                classes={{ root: classes.categoryChip }}
                label={categoryName.toUpperCase()}
                color='secondary'
                variant={chosenCategoryType.includes(categoryName) ? 'default' : 'outlined'}
                onClick={() => handleCategoryTypeUpdate(categoryName)}
            />
        );
    });

    // Wait for user to choose a type, then create chip for each subtype
    let categorySubTypeChips;
    if (
        categorySubTypes !== undefined
        && categorySubTypes.length !== 0
    ) {
        categorySubTypeChips = categorySubTypes.map(subTypeName => {
            return (
                <Chip
                    key={subTypeName}
                    classes={{ root: classes.categoryChip }}
                    label={subTypeName.toUpperCase()}
                    color='primary'
                    variant={chosencategorySubTypes.includes(subTypeName) ? 'default' : 'outlined'}
                    onClick={() => handleCategorySubtypeUpdate(subTypeName)}
                />
            )
        });
    }

    return (
        <Fragment>
            <TextField
                className={classes.search}
                id="input-with-icon-textfield"
                placeholder="Search"
                variant="outlined"
                InputProps={{
                startAdornment: (
                    <InputAdornment position="start">
                        <Search />
                    </InputAdornment>
                ),
                }}
                onChange={handleSearchChange}
            />

            <TuneIcon 
                className='icon' 
                onClick={() => updateProductsOptionsVisible(currentOption => !currentOption)}
            />

            <div id="optionsContainer" style={{ display: productsOptionsVisible ? 'none' : null }}>
                <Autocomplete
                    id='storeFilterSelect'
                    classes={{ root: classes.root }}
                    InputProps={{ className: classes.input }}
                    options={Object.keys(allUniqueStores).map(brand => brand)}
                    noOptionsText="No stores"
                    onChange={handleStoreFilter}
                    ListboxComponent={ListboxComponent}
                    getOptionLabel={(store) => store}
                    renderOption={(store, { selected }) => (
                        <Fragment>
                            <Checkbox
                                style={{ marginRight: '2px' }}
                                checked={selected}
                            />
                            <span className='storeFilterOption'>{store}</span>
                        </Fragment>
                    )}
                    renderInput={(params) => (
                        <TextField 
                            {...params} 
                            variant="outlined" 
                            label="Pick a brand" 
                            placeholder="Nike, Lululemon, Apple etc."
                        />
                    )}
                    limitTags={1}
                    multiple
                    disableCloseOnSelect
                />
                <div className='selectContainer'>
                    <InputLabel classes={{ root: classes.selectLabel }} id="sort-select-label">Sort</InputLabel>
                    <Select
                        classes={{ root: classes.select }}
                        labelId="sort-select-label"
                        id='sort-select'
                        value={currentSortOption}
                        onChange={handleSortChange}
                        variant='outlined'
                    >
                        <MenuItem value='default'>Default</MenuItem>
                        <MenuItem value='lowToHigh'>Low$-High$</MenuItem>
                        <MenuItem value='highToLow'>High$-Low$</MenuItem>
                        {/* <MenuItem value='A-Z'>A-Z</MenuItem> */}
                        {/* <MenuItem value='Z-A'>Z-A</MenuItem> */}
                    </Select>
                </div>
                { 
                    genderSelectVisible
                    ?   <div className='selectContainer'>
                            <InputLabel classes={{ root: classes.selectLabel }} id="gender-select-label">Gender</InputLabel>
                            <Select
                                labelId='gender-select-label'
                                id='gender-select'
                                value={genderChosen}
                                onChange={handleGenderFilter}
                                variant='outlined'
                            >
                                <MenuItem value='both'>Both</MenuItem>
                                <MenuItem value='male'>Men</MenuItem>
                                <MenuItem value='female'>Women</MenuItem>
                            </Select>
                        </div>
                    : null
                }
                <div id='categoryTypeContainer'>
                    {categoryTypesMenuItems}
                </div>
                {categorySubTypes.length !== 0
                ? <div id='subCategoriesContainer'>
                    {chosencategorySubTypes ? categorySubTypeChips : null}
                  </div>
                : null
                }
            </div>
        </Fragment>
    );
};

ProductsHeader.propTypes = {
    allUniqueStores: PropTypes.array.isRequired,
    handleStoreFilter: PropTypes.func.isRequired,
    currentSortOption: PropTypes.string.isRequired,
    handleSortChange: PropTypes.func.isRequired,
}

export default ProductsHeader;