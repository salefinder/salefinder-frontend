import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor'; // Trigger auto loads more products

// Utils
import { capitalize } from '../utils/strings';

// Material UI
import {
    Button,
} from '@material-ui/core';

// Styles
import '../styles/productsList.css';


// Renders products that it is given
function ProductsList(props) {
    const { 
        displayedProducts,
        remainingProducts,
        handleDisplayMoreProducts,
        handleVisibilityChange,
    } = props;

    return (
        <Fragment>
            {displayedProducts.length !== 0 
                ? displayedProducts.map((product, index) => {
                    const { url, name, brand, normal_price, sale_price } = product;

                    // Missing https:// for img request
                    let img_url = product.img_url;
                    if (!img_url.startsWith('https://') && !img_url.startsWith('http://')) {
                        img_url = `https://${img_url}`;
                    }

                    return (
                        <div key={`${name} - ${Math.floor(Math.random(0, 1) * 100000)}`} className='singleProduct'>
                            <div className='productImageContainer'>
                                <a href={url} target='_blank' rel='noopener noreferrer'>
                                    <img className='productImage' src={img_url} alt={name} />
                                </a>
                            </div>

                            <div className='productItemsContainer'>
                                {brand ? <div className='productBrand'>{capitalize(brand)}</div> : null}
                                
                                <div className='productNameContainer'>
                                    <a className='productName' href={url} target='_blank' rel='noopener noreferrer'>
                                        {name}
                                    </a>
                                </div>

                                <span className='productNormalPrice'>${normal_price}</span>                            
                                <div className='productSalePrice'>${sale_price}</div>
                            </div>

                            {
                                index === displayedProducts.length - 8 && displayedProducts.length !== remainingProducts
                                ? <VisibilitySensor onChange={handleVisibilityChange}>
                                    <Button onClick={handleDisplayMoreProducts}>
                                        Load more
                                    </Button>
                                </VisibilitySensor>
                                : null
                            }
                        </div>
                    )
                })
                : 'No products found with those critera'
            }
        </Fragment>
    );
};

ProductsList.propTypes = {
    displayedProducts: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductsList;