import React, { useState } from 'react';

// Material UI
import {
    List,
    ListItem,
    TextField,
    Button,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

// Email validation
import EmailValidator from 'email-validator';

// Styles
import '../styles/signUp.css';

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiFilledInput-root': {
            color: '#333333',
            fontSize: '20px',
            fontFamily: 'inherit',
        },
        '& .MuiFilledInput-underline::before': {
            borderColor: '#999999'
        },
        '& .MuiFilledInput-underline::after': {
            borderColor: '#e0208a'
        },
        '& .MuiButton-root': {
            marginTop: '10px',
            padding: '16px 15px 16px 15px',
            border: 0,
            color: '#ffffff',
            backgroundColor: '#e0208a',
            justifyContent: 'center',
            fontSize: '17px',
            float: 'right',
        },
        '& .MuiButton-root:hover': {
            backgroundColor: '#e754a5',
        },
        '& .MuiButton-root:disabled': {
            backgroundColor: '#50546b',
        },
        // Mobile screens
        [theme.breakpoints.down('sm')]: {
            '& .MuiFilledInput-root': {
                marginRight: 0,
                marginBottom: '5px',
                fontSize: '20px',
            },
            '& .MuiButton-root': {
                marginTop: '10px',
                fontSize: '15px',
                float: 'right',
                display: 'inline-block',
            },
        },
        justifyContent: 'flex-end'
      
    },
}));

function SignUp() {
    const classes = useStyles();

    return (
        <div id='signUpContainer'>
            <div id='signUpTitlesContainer'>
                <h1 id='signUpTitle'>The easiest way to find your favorite products on sale</h1>

                <List>
                    <ListItem className='listItem'>
                        <img className='icon' src='./shopping-bag.png' alt='shopping bag icon' />
                        <div className='listItemTextContainer'>
                            <div className='signUpSubTitle'>Your favorite stores</div>
                            <span className='signUpSubText'>We bring all your favorite stores to one place. Lululemon, American Threads, and Ulta are some examples</span>
                        </div>
                     </ListItem>
                    <ListItem className='listItem'>
                        <div className='listItemTextContainer'>
                            <div className='signUpSubTitle'>All items on sale</div>
                            <span className='signUpSubText'>Everything on our platform is on sale! The best deals on your favorite products right in front of you</span>
                        </div>
                        <img className='icon' src='./price.png' alt='price tag icon' />
                    </ListItem>
                    <ListItem className='listItem'>
                        <img className='icon' src='./time.png' alt='watch icon' />
                        <div className='listItemTextContainer'>
                            <div className='signUpSubTitle'>Less searching, more discovering</div>
                            <span className='signUpSubText'>Find products that your never knew existed by having everything in one place</span>
                        </div>
                    </ListItem>
                </List>
            </div>
            
            <img id='shoppingArt' src='./shoppingWoman.jpg' alt='woman shopping artwork' />
        </div>
    )
}

export default SignUp;