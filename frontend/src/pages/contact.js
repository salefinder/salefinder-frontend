import React, { useState } from 'react';
import '../styles/contact.css';

// Components
import Contact from '../components/Contact';

// Quick tools
import EmailValidator from 'email-validator'; // Email format

function ContactPage() {
    const [contactEmail, updateContactEmail] = useState('');
    const [contactMessage, updateContactMessage] = useState('');
    const [contactSuccess, updateContactSuccess] = useState(false);
    const [emailHasError, updateEmailHasError] = useState(false);
    const [messageHasError, updateMessageHasError] = useState(false);
    const [contactErrorMessage, updateContactErrorMessage] = useState(false);

    // Update state of contact email or message
    const handleFormUpdate = event => {
        const inputName = event.target.name;
        const inputValue = event.target.value;

        // Update email
        if (inputName === 'contactEmail') {
            updateContactEmail(inputValue);
        } else {
            // Update message
            updateContactMessage(inputValue);
        }

        // Clear error message if everything is good
        if (contactEmail && contactMessage && EmailValidator.validate(contactEmail)) {
            updateContactErrorMessage(false);
            updateEmailHasError(false);
            updateMessageHasError(false);
            return;
        }

        // Clear email input errors
        if (contactEmail && EmailValidator.validate(contactEmail)) {
            updateEmailHasError(false);
        }

        // Clear message input errors
        if (contactMessage) {
            updateMessageHasError(false);
        }
    };

    const handleSubmit = () => {
        if (!contactEmail || !contactMessage) {
            updateContactErrorMessage('Please fill out both forms');
            updateEmailHasError(true);
            updateMessageHasError(true);
            return
        } else if (!EmailValidator.validate(contactEmail)) {
            updateContactErrorMessage('Invalid email address');
            updateEmailHasError(true);
            return;
        } else {
            // Clear error messages
            updateContactErrorMessage(false);
            updateEmailHasError(false);
            updateMessageHasError(false);
        }

        // Send message to api
        const url = 'https://dealyy.com/api/messages/create';
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email: contactEmail, message: contactMessage })
        })
        .then(response => {
            if (![200,201].includes(response.status)) {
                throw response;
            }
            
            // Update success message
            updateContactSuccess(true);
        })
        .catch(error => {
            console.log(error);
            updateContactErrorMessage('There was an error while sending your message, please try again')
        });
    };

    return (
        <div id='contactBodyContainer'>
            <img id='talkingPeopleImage' src='./talkingPeople.jpg' alt='Talking people' />
            <Contact
                handleFormUpdate={handleFormUpdate}
                handleSubmit={handleSubmit}
                emailHasError={emailHasError}
                messageHasError={messageHasError}
                contactErrorMessage={contactErrorMessage}
                contactSuccess={contactSuccess}
            />
        </div>
    );
}

export default ContactPage;