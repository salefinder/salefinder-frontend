import React, { Fragment } from 'react';

// Components
import Products from '../components/Products';
import SignUp from '../components/SignUp';

function HomePage() {
    return (
        <Fragment>
            <SignUp />
            <Products />
        </Fragment>
    );
}

export default HomePage;
