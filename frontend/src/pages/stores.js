import React, { Fragment } from 'react';
import '../styles/stores.css';

// Components


function StoresPage() {
    const logos = [
        'choosy',
        'victoriaSecret',
        'guess',
        'ulta',
        'lululemon',
        'target',
        'ralphLauren',
        'nike',
        'americanThreads',
    ];

    return (
        <Fragment>
            <div id='storesContainer'>
                <div id='logosContainer'>
                    <h1 id='storesTitle'>Here's a list of the stores we support</h1>
                    {
                        logos.map(store => {
                            return <img className='storeLogo' src={`./logos/logo_${store}.jpg`} alt={`${store} logo`} />
                        })
                    }
                </div>
                <img id='windowShoppingArt' src='./windowShopping.png' alt='Window shopping art' />
            </div>
        </Fragment>
    );
}

export default StoresPage;