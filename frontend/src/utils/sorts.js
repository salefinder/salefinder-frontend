// A-Z sort by product name
function aTozSort(array) {
    return array.sort((a, b) => a.name.localeCompare(b.name))
}

// Z-A sort by product name
function zToASort(array) {
    return array.sort((a, b) => b.name.localeCompare(a.name))
}

// Low -> High sort by product name
function lowToHighSort(array) {
    return array.sort((a, b) => parseFloat(a.sale_price) - parseFloat(b.sale_price))
}

// High -> Low sort by product name
function highToLowSort(array) {
    return array.sort((a, b) => parseFloat(b.sale_price) - parseFloat(a.sale_price))
}

// Random mix up items in product list
function randomSort(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
}

// Choose sort type based on dropdown selection
export const sortProducts = (currentSortOption, unsortedArray) => {
    let sortedProducts;

    switch (currentSortOption) {
        case 'A-Z':
            sortedProducts = aTozSort(unsortedArray);
            break;
        case 'Z-A':
            sortedProducts = zToASort(unsortedArray);
            break;
        case 'random':
            sortedProducts = randomSort(unsortedArray);
            break;
        case 'lowToHigh':
            sortedProducts = lowToHighSort(unsortedArray);
            break;
        case 'highToLow':
            sortedProducts = highToLowSort(unsortedArray);
            break;
        default:
            sortedProducts = unsortedArray;
            break;
    };

    return sortedProducts;
};